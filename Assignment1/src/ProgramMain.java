
import Attributes.PrimaryAttributes;
import Equipment.*;
import Character.*;
import Equipment.Exceptions.InvalidArmorException;
import Equipment.Exceptions.InvalidWeaponException;

public class ProgramMain {
    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {

        Weapon bow = new Weapon("AlexWander",1,Slot.Weapon,WeaponType.Wand,2,1.1);
        Armor helmet = new Armor("Helmet",1,Slot.Head,ArmorType.Cloth,new PrimaryAttributes(3,1,1,1));
        Armor leggins = new Armor("Leggins",1,Slot.Legs,ArmorType.Cloth,new PrimaryAttributes(6,1,1,1));
        Armor helmet2 = new Armor("Helmet",1,Slot.Head,ArmorType.Cloth,new PrimaryAttributes(3,1,1,1));
        Mage wizard = new Mage("Dr strange");

        wizard.levelUp(1);
        //wizard.levelUp(3);
        wizard.setArmor(helmet);
        wizard.setArmor(leggins);
        wizard.setArmor(helmet2);
        wizard.setWeapon(bow);
        wizard.getInfo();
        wizard.levelUp(2);
        wizard.getInfo();

    }
}
