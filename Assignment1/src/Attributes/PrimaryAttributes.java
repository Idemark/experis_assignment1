package Attributes;

public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
    /*
    Checks if the primary attributes are equal to the expected attributes if you for example create a character or levelUp
    Mostly used in testing
     */
    public boolean isEqual(PrimaryAttributes a) {
        if(a instanceof PrimaryAttributes){
            if(this.strength == a.getStrength() &&this.dexterity == a.getDexterity()&&this.intelligence == a.getIntelligence() && this.vitality == a.getVitality()) {
                return true;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }
}
