package Attributes;
import Character.*;
public class SecondaryAttributes {
    private int health =10;
    private int armorRating;
    private int elemantalResistance;

    public SecondaryAttributes(PrimaryAttributes primaryAttributes) {
        this.health = health*primaryAttributes.getVitality();
        this.armorRating = (primaryAttributes.getStrength()+ primaryAttributes.getDexterity());
        this.elemantalResistance = primaryAttributes.getIntelligence();
    }
    public SecondaryAttributes(int health,int armorRating,int elemantalResistance){
        this.health=health;
        this.armorRating=armorRating;
        this.elemantalResistance=elemantalResistance;
    }
    /*
   Checks if the secondary attributes are equal to the expected attributes if you for example create a character or levelUp
   Mostly used in testing
    */
    public boolean isEqual(SecondaryAttributes a) {
        if (a instanceof SecondaryAttributes) {
            if (this.health == a.getHealth() && this.armorRating == a.getArmorRating() && this.elemantalResistance == a.getElemantalResistance()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public int getHealth() { return health; }

    public int getArmorRating() { return armorRating; }

    public int getElemantalResistance() { return elemantalResistance; }

    public void setHealth(PrimaryAttributes primaryAttributes) { this.health = 10*primaryAttributes.getVitality(); }

    public void setArmorRating(PrimaryAttributes primaryAttributes) { this.armorRating = (primaryAttributes.getStrength()+ primaryAttributes.getDexterity()); }

    public void setElemantalResistance(PrimaryAttributes primaryAttributes) { this.elemantalResistance = primaryAttributes.getIntelligence(); }
}
