package Character;

import Equipment.*;
import Attributes.*;
import Equipment.Exceptions.InvalidArmorException;
import Equipment.Exceptions.InvalidWeaponException;
import java.util.HashMap;

abstract class Characters {
    private String name;
    private int level;
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;
    private PrimaryAttributes basePrimAttr;
    private PrimaryAttributes totalPrimAttr;
    private SecondaryAttributes secoAttr;
    private Armor armor;
    protected Weapon weapon;
    private double characterDPS;
    HashMap<Slot,Item> equipment = new HashMap<Slot,Item>();

    /*
    *Sets up the all necessary variables for each character
     */
    public Characters(String name, int strength,int dexterity,int intelligence,int vitality,double primaryAttribute) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
        this.level =1;
        this.name = name;
        this.basePrimAttr = new PrimaryAttributes(this.strength,this.dexterity,this.intelligence,this.vitality);
        this.totalPrimAttr = new PrimaryAttributes(this.strength,this.dexterity,this.intelligence,this.vitality);
        this.secoAttr = new SecondaryAttributes(totalPrimAttr);
        this.characterDPS = 1 * ((1+((primaryAttribute/100)))); // The main primary attribute are unique for each character
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLevel(int level){
        this.level=level;
    }

    public int getLevel() {
        return level;
    }

    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException{
        this.weapon = weapon;
        return true;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public boolean setArmor(Armor armor) throws InvalidArmorException {
        this.armor = armor;
        return true;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setCharacterDPS(double characterDPS) {
        this.characterDPS = characterDPS;
    }

    public double getCharacterDPS() {
        return characterDPS;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    // Adds a unique weapon to the hashmap. If another weapon is assigned it writes over the old one.
    public void Equip(Weapon weapon) { this.equipment.put(weapon.getSlot(),weapon); }

    // Adds a unique armor to the hashmap. If another armor for the same slot is assigned it writes over the old one.
    public void Equip(Armor armor) {
        this.equipment.put(armor.getSlot(), armor);
    }

    public PrimaryAttributes getBasePrimAttr() {
        return basePrimAttr;
    }

    public void setBasePrimAttr(PrimaryAttributes basePrimAttr) {
        this.basePrimAttr = basePrimAttr;
    }

    public PrimaryAttributes getTotalPrimAttr() {
        return totalPrimAttr;
    }

    public SecondaryAttributes getSecoAttr() {
        return secoAttr;
    }

    /*
    Method for leveling up a character. The amount of levels a character levels up is set by parameter level.
    Note that the character cant have a parameter equal or below zero. This will throw a illegalArgumentExcpetion
    It loops the amount of levels that the character should levelUp and changes the primary and secondary attributes to
    fit the new level. Than it update the totalAttributes.
     */
    public void levelUpHero(PrimaryAttributes levelUpHero, int level){
        if(level >0) {
            PrimaryAttributes levelUp = levelUpHero;
            for(int i=0; i<level; i++) {
                this.level++;
                basePrimAttr.setStrength(basePrimAttr.getStrength() + levelUp.getStrength());
                basePrimAttr.setDexterity(basePrimAttr.getDexterity() +levelUp.getDexterity());
                basePrimAttr.setIntelligence(basePrimAttr.getIntelligence()+levelUp.getIntelligence());
                basePrimAttr.setVitality((basePrimAttr.getVitality()+levelUp.getVitality()));
                secoAttr.setHealth(basePrimAttr);
                secoAttr.setElemantalResistance(basePrimAttr);
                secoAttr.setArmorRating(basePrimAttr);
            }
            updateTotal();
        }
        else {
            throw new IllegalArgumentException("The levelUp needs to be greater then 0");
        }
    }
    /*
    Updates the attributes every time a new armor or if a character leveled up
    It checks if any equipment are assigned to the hashmap. If it is it adds those attributes to the total.
    It also updates the secondary attributes when armor are assigned.
     */
    public void updateTotal() {
        int strength = basePrimAttr.getStrength();
        int dexterity = basePrimAttr.getDexterity();
        int intelligence = basePrimAttr.getIntelligence();
        int vitality = basePrimAttr.getVitality();
        for(Item item : equipment.values()) {
            if(item instanceof Armor){
                Armor armor = (Armor) item;
                strength += armor.getArmorAttributes().getStrength();
                dexterity += armor.getArmorAttributes().getDexterity();
                intelligence += armor.getArmorAttributes().getIntelligence();
                vitality += armor.getArmorAttributes().getIntelligence();
            }
        }
        this.totalPrimAttr = new PrimaryAttributes(strength,dexterity,intelligence,vitality);
        secoAttr.setHealth(totalPrimAttr);
        secoAttr.setElemantalResistance(totalPrimAttr);
        secoAttr.setArmorRating(totalPrimAttr);
    }

    /*
    Displays a list of the characters stats
     */
    public void getInfo() {
        System.out.println("----------------------------"+ "\n"+"Level " + getLevel() +"\n"+ "----------------------------\n" +
                "Name: " + getName() + "\n" + "Level: " +getLevel() + "\n" + "Strength: "  +
                totalPrimAttr.getStrength() + "\n" + "Dexterity: " + totalPrimAttr.getDexterity() + "\n"
                + "Vitality: " +totalPrimAttr.getVitality() + "\n" + "Intelligence: " + totalPrimAttr.getIntelligence() + "\n" +
                "Health: "+secoAttr.getHealth() + "\n" + "Armor Rating: " +secoAttr.getArmorRating() + "\n"
                + "Elemental Resistance: " + secoAttr.getElemantalResistance()+"\n"+"DPS: " + characterDPS +"\n");
    }
}