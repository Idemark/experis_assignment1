package Character;

import Attributes.PrimaryAttributes;
import Equipment.Armor;
import Equipment.ArmorType;
import Equipment.Exceptions.InvalidArmorException;
import Equipment.Exceptions.InvalidWeaponException;
import Equipment.Weapon;
import Equipment.WeaponType;

public class Ranger extends Characters {

    public Ranger(String name) {
        super(name,1,7,1,8,7); //Creating a character with the specific attributes for a ranger

    }
    /*
    Sends with the unique levelUp attributes and amount of levels to for a specific character to the Characters class
     */
    public void levelUp(int levelUp) {
        levelUpHero(new PrimaryAttributes(1,5,1,2),levelUp);
    }

    /*
    Checks if the weapon-type and required level are right. If so it updates the character DPS and equips the weapon to its hashmap.
    If something isn't right it throws a invalidWeaponException
     */
    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getWeaponType() == WeaponType.Bow)
            if(getLevel() < weapon.getLevel()) {
                throw new InvalidWeaponException("Mage characters level is to low for this weapon");
            }
            else {
                setCharacterDPS(weapon.getDPS() * ((1+((double)getTotalPrimAttr().getDexterity()/100))));
                Equip(weapon);
                super.weapon = weapon;
                return true;
            }
        else{
            throw new InvalidWeaponException("Cant add this weapontype to mage character!");
        }
    }

    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public Armor getArmor() {
        return super.getArmor();
    }

    @Override
    /*
    Checks if the armor-type and required level are right. If so it updates the character DPS and equips the armor to its hashmap.
    If something isn't right it throws a invalidArmorException
    If a character doesn't have a weapon it sets the Weapon DPS to 1.
     */
    public boolean setArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType() == ArmorType.Leather || armor.getArmorType() == ArmorType.Mail){
            if (getLevel() < armor.getLevel()){
                throw new InvalidArmorException("Mage characters level is to low for this armor");
            }
            else {
                Equip(armor);
                super.setArmor(armor);
                updateTotal();
                if(super.getWeapon() != null) {
                    setCharacterDPS(super.getWeapon().getDPS() * ((1 + ((double) getTotalPrimAttr().getDexterity() / 100))));
                }
                else {
                    setCharacterDPS(1 * ((1 + ((double) getTotalPrimAttr().getDexterity() / 100))));
                }
                return true;
            }
        }
        else{
            throw new InvalidArmorException("Cant add this Armortype to a Mage character");
        }
    }
}
