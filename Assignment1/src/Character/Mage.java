package Character;

import Equipment.*;
import Attributes.*;
import Equipment.Exceptions.InvalidArmorException;
import Equipment.Exceptions.InvalidWeaponException;

public class Mage extends Characters{

    public Mage(String name) {
        super(name,1,1,8,5,8); //Creating a character with the specific attributes for a mage

    }
    /*
    Sends with the unique levelUp attributes and amount of levels to for a specific character to the Characters class
     */
    public void levelUp(int levelUp) {
        levelUpHero(new PrimaryAttributes(1,1,5,3),levelUp);
    }

    /*
    Checks if the weapon-type and required level are right. If so it updates the character DPS and equips the weapon to its hashmap.
    If something isn't right it throws a invalidWeaponException
     */
    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getWeaponType() == WeaponType.Wand || weapon.getWeaponType() == WeaponType.Staff)
            if(getLevel() < weapon.getLevel()) {
                throw new InvalidWeaponException("Mage characters level is to low for this weapon");
            }
            else {
                setCharacterDPS(weapon.getDPS() * ((1+((double)getTotalPrimAttr().getIntelligence()/100))));
                Equip(weapon);
                super.weapon = weapon;
                return true;
            }
        else{
            throw new InvalidWeaponException("Cant add this weapontype to mage character!");
        }
    }

    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public Armor getArmor() {
        return super.getArmor();
    }

    /*
    Checks if the armor-type and required level are right. If so it updates the character DPS and equips the armor to its hashmap.
    If something isn't right it throws a invalidArmorException
    If a character doesn't have a weapon it sets the Weapon DPS to 1.
     */
    @Override
    public boolean setArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType() == ArmorType.Cloth){
            if (getLevel() < armor.getLevel()){
                throw new InvalidArmorException("Mage characters level is to low for this armor");
            }
            else {
                Equip(armor);
                super.setArmor(armor);
                updateTotal();
                if(super.getWeapon() != null) {
                    setCharacterDPS(super.getWeapon().getDPS() * ((1 + ((double) getTotalPrimAttr().getIntelligence() / 100))));
                }
                else {
                    setCharacterDPS(1 * ((1 + ((double) getTotalPrimAttr().getIntelligence() / 100))));
                }
                return true;
            }
        }
        else{
            throw new InvalidArmorException("Cant add this Armortype to a Mage character");
        }
    }
}
