package Equipment;

import Attributes.PrimaryAttributes;

public class Armor extends Item{
    private ArmorType armorType;
    private PrimaryAttributes armorAttributes;

    public Armor(String name,int level, Slot slot, ArmorType armorType, PrimaryAttributes armorAttributes) {
        super(name, level,slot);
        this.armorType = armorType;
        this.armorAttributes = armorAttributes;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorAttributes(PrimaryAttributes armorAttributes) { this.armorAttributes = armorAttributes; }

    public PrimaryAttributes getArmorAttributes() {
        return armorAttributes;
    }
}
