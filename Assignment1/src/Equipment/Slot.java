package Equipment;

public enum Slot {
    Head,
    Body,
    Legs,
    Weapon;
}
