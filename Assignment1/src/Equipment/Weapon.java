package Equipment;

public class Weapon extends Item{
    private WeaponType weaponType;
    private int damage;
    private double attackDamage;
    private double DPS;


    public Weapon(String name, int level, Slot slot, WeaponType weaponType, int damage, double attackDamage) {
        super(name,level,slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackDamage = attackDamage;
        this.DPS = damage*attackDamage;
    }

    public double getDPS() {
        return DPS;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setAttackDamage(double attackDamage) {
        this.attackDamage = attackDamage;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackDamage() {
        return attackDamage;
    }
}
