package Equipment.Exceptions;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String errorMessage) { //Sends an error messages when an invalid armor is equiped.
        super(errorMessage);
    }
}
