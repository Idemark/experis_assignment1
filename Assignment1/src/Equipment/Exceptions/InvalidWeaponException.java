package Equipment.Exceptions;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String errorMessage) { //Sends an error messages when an invalid weapon is equiped.
        super(errorMessage);
    }
}
