import Attributes.PrimaryAttributes;
import Equipment.Exceptions.InvalidArmorException;
import Equipment.Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;
import Character.*;
import Equipment.*;
import static org.junit.jupiter.api.Assertions.*;

class ItemsAndEquipmentTests {
    @Test
    void TestEquipWeaponToWarrior_ToHighLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",2,Slot.Weapon,WeaponType.Axe,3,2);
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(axe));
    }
    @Test
    void TestEquipArmorToWarrior_ToHighLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",2,Slot.Body,ArmorType.Plate,new PrimaryAttributes(1,1,1,1));
        assertThrows(InvalidArmorException.class, () -> warrior.setArmor(body));
    }
    @Test
    void TestEquipWeaponToWarrior_WrongWeaponType_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Weapon bow = new Weapon("bow",1,Slot.Weapon,WeaponType.Bow,3,2);
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(bow));
    }
    @Test
    void TestEquipArmorToWarrior_WrongArmorType_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Cloth,new PrimaryAttributes(1,1,1,1));
        assertThrows(InvalidArmorException.class, () -> warrior.setArmor(body));
    }
    @Test
    void TestEquipWeaponToWarrior_ValidWeaponType_ShouldPass() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Warrior");
        Weapon sword = new Weapon("sword",1,Slot.Weapon,WeaponType.Sword,3,2);
        assertTrue(warrior.setWeapon(sword) ==true);
    }
    @Test
    void TestEquipArmorToWarrior_ValidArmorType_ShouldPass() throws InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Mail,new PrimaryAttributes(1,1,1,1));
        assertTrue(warrior.setArmor(body)==true);
    }
    @Test
    void TestCalculateDPS_ifNoWeaponAttachedProperly_ShouldPass() {
        Warrior warrior = new Warrior("Warrior");
        double expected = 1*(1+(5.0/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
    @Test
    void TestCalculateDPS_ifAxeWeaponAttachedProperly_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",1,Slot.Weapon,WeaponType.Axe,7,1.1);
        warrior.setWeapon(axe);
        double expected = (7*1.1) * (1+((5.0)/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
    @Test
    void TestCalculateDPS_ifWeaponAndArmorAttachedProperly_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",1,Slot.Weapon,WeaponType.Axe,7,1.1);
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Plate,new PrimaryAttributes(1,0,0,2));
        warrior.setWeapon(axe);
        warrior.setArmor(body);
        double expected = (7*1.1) * (1+((5.0+1)/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
}