import Attributes.PrimaryAttributes;
import Attributes.SecondaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import Character.*;
class AttributesAndLevelTests {

    @Test
    void TestCreateCharacter_isLevelOne_true() {
        Mage mage = new Mage("Gandalf");
        assertTrue(mage.getLevel() == 1);
    }

    @Test
    void TestLevelUpCharacter_levelUp_true() {
        Mage mage = new Mage("Gandalf");
        mage.levelUp(1);
        assertTrue(mage.getLevel() == 2);
    }
    @Test
    void TestLevelUpCharacter_lessThanOne_throwException() {
        Mage mage = new Mage("Gandalf");
        assertThrows(IllegalArgumentException.class, () -> mage.levelUp(-22));
    }
    @Test
    void TestCreateMageCharacter_ifValidPrimAttributes_true() {
        Mage mage = new Mage("Gandalf");
        PrimaryAttributes expected =  new PrimaryAttributes(1,1,8,5);
        PrimaryAttributes actual = mage.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestCreateRangerCharacter_ifValidPrimAttributes_true() {
        Ranger ranger = new Ranger("ranger");
        PrimaryAttributes expected =  new PrimaryAttributes(1,7,1,8);
        PrimaryAttributes actual = ranger.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestCreateRogueCharacter_ifValidPrimAttributes_true() {
        Rogue rogue = new Rogue("rogue");
        PrimaryAttributes expected =  new PrimaryAttributes(2,6,1,8);
        PrimaryAttributes actual = rogue.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestCreateWarriorCharacter_ifValidPrimAttributes_true() {
        Warrior warrior = new Warrior("Warrior");
        PrimaryAttributes expected =  new PrimaryAttributes(5,2,1,10);
        PrimaryAttributes actual = warrior.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestLevelingUpMageCharacter_ifPrimAttributesUpdated_true() {
        Mage mage = new Mage("Mage");
        mage.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(2,2,13,8);
        PrimaryAttributes actual = mage.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestLevelingUpRangerCharacter_ifPrimAttributesUpdated_true() {
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(2,12,2,10);
        PrimaryAttributes actual = ranger.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestLevelingUpRogueCharacter_ifPrimAttributesUpdated_true() {
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(3,10,2,11);
        PrimaryAttributes actual = rogue.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestLevelingUpWarriorCharacter_ifPrimAttributesUpdated_true() {
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(8,4,2,15);
        PrimaryAttributes actual = warrior.getBasePrimAttr();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void TestLevelingUpWarriorCharacter_isSecondaryAttributesUpdated_true() {
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp(1);
        SecondaryAttributes expected = new SecondaryAttributes(150,12,2);
        SecondaryAttributes actual = warrior.getSecoAttr();
        assertTrue(actual.isEqual(expected));

    }



}