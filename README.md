# RPG Character Console Application
**Intro)**<br/>
For this assignment the mission was to build a console application in java. The program should be able to create four different heroes/characters (Mage,Ranger,Rogue,Warrior). Every different type of character/hero has specific primary (Strength,Dexterity,Intelligence,Vitality) and secondary attributes (Health,Armor rating, Elemental resistance). The characters should be able to level up and update those attributes. The characters should also be able to equip four different slots containing armor for head, body and legs and a weapon. Every character has specific rules for what armor-types and weapons-types that can be equipped. If the rules aren't followed the program will throw an exception. 
 
**Armor)**<br/>
The armor contains primary attributes that should be added to the characters total primary attributes (containing the base primary attributes + the primary attributes from the armor). The armor contains four different types(Cloth,Leather,Mail,Plate) and the armor are restricted to certain characters. This is shown below:
 
* Mage - Cloth<br/>
* Rangers – Leather, Mail<br/>
* Rogues – Leather, Mail<br/>
* Warriors – Mail, Plate<br/>
 
**Weapon)**<br/>
Weapons have a base damage and an attack speed (attack per seconds). This is to be able to calculate a DPS (damage per second), to rate how well the weapon performs. If a character hasn't equipped any weapon the DPS is set to 1. The weapon contains seven different types (Axe,Bow,Dagger,Hammer,Staff,Sword,Wand) and the weapons are restricted to certain characters. This is shown below:
 
* Mages – Staff, Wand<br/>
* Rangers – Bow<br/>
* Rogues – Dagger, Sword<br/>
* Warriors – Axe, Hammer, Sword<br/>
 
The characters have an DPS (damage per seconds) that is calculated by the following equation:<br/>
 
 
* **Character DPS = Weapon DPS * (1 + TotalPrimaryAttribute/100)**<br/>
 
Every character DPS increases with 1% when their primary attribute increases (Intelligence for mage) or with a more powerful weapon. <br/>
 
**Unit testing)**<br/>
For testing some functionality in the program, two different tests were made. One test named _AttributesAndLevelTest_, that tests how well a character object updates their attributes when it is created and when it levels up. The other test named _ItemsAndEquipment_ that tests how well the program throws exceptions if for example a weapon with a higher required level than the level of a character. It also tests how well the attributes and DPS updates when an armor or/and a weapon is equipped to the character.
 
**Project Structure)**<br/>
The project includes three packages named _Attributes, Character and Equipment_. The Attributes package holds two classes _primary attributes_ and _secondary attributes_ and these hold all the attributes that the characters should have. In the Character package an abstract class named _characters_ is made, that holds all common methods for all different types of characters. The package also contains four subclasses with the different types of characters _Mage, Ranger, Rogue and Warrior_. In the last package _Equipment_ it contains one package with selfmade exceptions and six classes (_Armor,ArmorType,Item,Slot,Weapon,WeaponType_). Those classes are used to make weapon and armor-object for the characters.  
